import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-life-shifter',
  templateUrl: './life-shifter.component.html',
  styleUrls: ['./life-shifter.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LifeShifterComponent implements OnInit {
  

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.loadValues();
    this.lifewarn = this._life <= (this.maxLife/3);
  }

  private storeValues() {
    this.localStorageService.set('life', this._life);
    this.localStorageService.set('maxLife', this._maxLife);
  }

  private loadValues() {
    var keys = this.localStorageService.keys();
    if (keys.indexOf('life') != -1) {
      this._life = this.localStorageService.get('life');
    }
    if (keys.indexOf('maxLife') != -1) {
      this._maxLife = this.localStorageService.get('maxLife');
    }
  }

  _life = 0;
  _maxLife = 0;
  lifewarn = this._life <= (this.maxLife/3);

  set life (life){
    this._life = life;
    this.lifewarn = this._life <= (this.maxLife/3)
    this.storeValues();
  }
  get life (): number{
    return this._life;
  } 
  
  set maxLife (maxLife){
    this._maxLife = maxLife;
    this.storeValues();
  }
  get maxLife (): number{
    return this._maxLife;
  }
  

}
